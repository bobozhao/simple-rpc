package listener;

/**
 * @author : zGame
 * @version V1.0
 * @Project: simple-rpc
 * @Package listener
 * @Description: TODO
 * @date Date : 2022年02月28日 11:06
 */
public interface ITaskFinishListener {

    public void taskFinish(byte type,String taskId);
}
