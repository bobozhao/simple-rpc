package message.rpcReq;

import lombok.Getter;
import lombok.Setter;
import message.IMessage;

/**
 * @author : zGame
 * @version V1.0
 * @Project: simple-rpc
 * @Package message
 * @Description: TODO
 * @date Date : 2022年02月28日 11:40
 */
@Getter
@Setter
public class PlayerMessage implements IMessage {
    private int messageId = 1001;
    private String msgData = "PlayerMessage_Content";

}
