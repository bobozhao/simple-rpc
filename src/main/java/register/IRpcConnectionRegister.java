package register;


/**
 * @author : zGame
 * @version V1.0
 * @Project: simple-rpc
 * @Package PACKAGE_NAME
 * @Description: TODO
 * @date Date : 2022年02月28日 11:20
 */
public interface IRpcConnectionRegister {
    
    public void registerConnection(String name,String host,int port);
    
}
