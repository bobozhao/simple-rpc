package rpc;

import listener.ITaskFinishListener;
import lombok.Getter;

import java.util.concurrent.CompletableFuture;

/**
 * @author : zGame
 * @version V1.0
 * @Project: simple-rpc
 * @Package rpc
 * @Description: TODO
 * @date Date : 2022年02月28日 18:59
 */
@Getter
public class CompletableRpcTask extends CompletableFuture {
    
    private String taskId;
    
    private ITaskFinishListener listener;
    
    public CompletableRpcTask(String taskId, ITaskFinishListener listener) {
        this.taskId = taskId;
        this.listener = listener;
    }
}
