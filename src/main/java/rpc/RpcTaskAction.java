package rpc;

import message.IMessage;

import java.util.concurrent.TimeUnit;

/**
 * @author : zGame
 * @version V1.0
 * @Project: simple-rpc
 * @Package rpc
 * @Description: TODO
 * @date Date : 2022年02月28日 11:13
 */
public interface RpcTaskAction{
    public <T> T get(IMessage message);
    public <T>T get(IMessage message,long time, TimeUnit timeUnit);
}
