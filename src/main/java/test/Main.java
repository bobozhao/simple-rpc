package test;

import message.rpcReq.PlayerMessage;
import rpc.RpcClient;

/**
 * @author : zGame
 * @version V1.0
 * @Project: simple-rpc
 * @Package test
 * @Description: TODO
 * @date Date : 2022年02月28日 14:19
 */
public class Main {
    
    public static void main(String[] args) {
        
        String name = "rpcServer1";
        String host = "127.0.0.1";
        int port = 8088;
        RpcClient.getInstance().registerConnection(name, host, port);
        long begin = System.currentTimeMillis();
        String msg = RpcClient.getInstance().get(new PlayerMessage());
        System.out.println(msg + " 耗时:" + (System.currentTimeMillis() - begin));
    }
}
